class Empleado:
	def __init__(self, nombre, nomina): #es el constructor, metodo, 'tiene' - atributos
		self.nombre = nombre
		self.nomina = nomina

	def calculo_impuestos(self):
		impuestos = self.nomina * 0.30
		return impuestos
		
	def __str__(self): #para hacer print se usa este metodo
		return "El empleado {name} debe pagar {tax:.2f}".format(name=self.nombre,
		tax=self.calculo_impuestos())
	
empleadoPepe = Empleado("Pepe", 20000) 
empleadaAna = Empleado("Ana",30000)

total = empleadoPepe.calculo_impuestos() + empleadaAna.calculo_impuestos()

print(empleadoPepe)
print(empleadaAna)
print("Los impuestos a pagar en total son {:.2f} euros".format(total))

